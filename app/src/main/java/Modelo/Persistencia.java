package Modelo;

import com.example.examenc2.Ventas;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertVenta(Ventas venta);

}
