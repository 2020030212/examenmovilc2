package com.example.examenc2;

public class BombaGasolina {
    int id;
    int numBomba;
    int capacidadBomba;
    int tipoGasolina;
    float precioGasolina;
    int acumuladorLitrosBomba;

    public BombaGasolina(int id, int numBomba, int capacidadBomba, int tipoGasolina, float precioGasolina, int acumuladorLitrosBomba){
        this.id = id;
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.tipoGasolina =tipoGasolina;
        this.precioGasolina = precioGasolina;
        this.acumuladorLitrosBomba = acumuladorLitrosBomba;
    }

    public BombaGasolina(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public int getCapacidadBomba() {
        return capacidadBomba;
    }

    public void setCapacidadBomba(int capacidadBomba) {
        this.capacidadBomba = capacidadBomba;
    }

    public int getTipoGasolina() {
        return tipoGasolina;
    }

    public void setTipoGasolina(int tipoGasolina) {
        this.tipoGasolina = tipoGasolina;
    }

    public float getPrecioGasolina() {
        return precioGasolina;
    }

    public void setPrecioGasolina(float precioGasolina) {
        this.precioGasolina = precioGasolina;
    }

    public int getAcumuladorLitrosBomba() {
        return acumuladorLitrosBomba;
    }

    public void setAcumuladorLitrosBomba(int acumuladorLitrosBomba) {
        this.acumuladorLitrosBomba = acumuladorLitrosBomba;
    }

    public int inventario(){
        return getCapacidadBomba() - getAcumuladorLitrosBomba();
    }

    public float hacerVenta(int cantidad){
        return getCapacidadBomba() - cantidad;
    }
}