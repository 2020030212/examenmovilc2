package com.example.examenc2;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private EditText editTextBomba, editTextPrecio, editTextCapacidad, editTextContador;
    private RadioGroup radioGroupGasolina;
    private Button buttonIniciarBomba, btnVenta, btnLimpiar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextBomba = findViewById(R.id.editTextBomba);
        editTextPrecio = findViewById(R.id.editTextPrecio);
        editTextCapacidad = findViewById(R.id.editTextCapacidad);
        editTextContador = findViewById(R.id.editTextContador);
        radioGroupGasolina = findViewById(R.id.radioGroupGasolina);
        buttonIniciarBomba = findViewById(R.id.buttonIniciarBomba);
        btnVenta = findViewById(R.id.btnVenta);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnSalir = findViewById(R.id.btnSalir);



        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextBomba.setText("");
                radioGroupGasolina.clearCheck();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}

